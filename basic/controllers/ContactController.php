<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Contact;
use app\models\Phone;

class ContactController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionList($id = 0) {
        $id = intval($id);

        if ($id) {
            $contacts[0] = Contact::find()->where(['id' => $id, 'active' => 1, 'deleted' => 0])->asArray()->one();
        } else {
            $contacts = Contact::find()->where(['active' => '1', 'deleted' => '0'])->asArray()->all();
        }

        if (!empty($contacts[0])) {
            foreach ($contacts as $k=>$contact) {
                $contacts[$k]['create_time'] = date("d.m.Y H:i:s",$contacts[$k]['create_time']);
                $contacts[$k]['edit_time'] = isset($contacts[$k]['edit_time'])? date("d.m.Y H:i:s",$contacts[$k]['edit_time']) : '';
                $contacts[$k]['phones_count'] = $this->GetCountPhoneNums($contact['id']);
            }
            return json_encode($contacts);
        }

        return json_encode(array(
            'error' => 'No records found',
            'error_code' => 4
        ));
    }


    public function actionAdd() {
        $model = new Contact();
        return $this->edit_contact($model, 1);
    }


    public function actionEdit($id = 0) {
        $id = intval($id);
        $model = new Contact();

        if ($id) {
            $model = Contact::find()->where(['id' => $id, 'deleted' => 0])->one();
            if ($model) {
                return $this->edit_contact($model);
            }

            return json_encode(array(
                'error' => 'Record not found or deleted',
                'error_code' => 4
            ));
        }

        return json_encode(array(
            'error' => "Invalid parameter id",
            'error_code' => 3
        ));

    }


    public function actionDelete($id = 0) {
        $id = intval($id);

        if ($id) {
            $model = Contact::find()->where(['id' => $id, 'deleted' => 0])->one();
            if ($model) {
                $model->deleted = 1;
                if ($model->save()) {
                    return json_encode(array(
                        'success' => "Your data was deleted successfully"
                    ));
                }

                return json_encode(array(
                    'error' => "Error while deleting",
                    'error_code' => 1
                ));
            }

            return json_encode(array(
                'error' => 'Record not found or deleted',
                'error_code' => 4
            ));
        }

        return json_encode(array(
            'error' => "Invalid parameter id",
            'error_code' => 3
        ));

    }


    public function GetCountPhoneNums($id = 0) {
        $id = intval($id); // contact_id

        if ($id && Contact::find()->where(['id' => $id, 'active' => 1, 'deleted' => 0])->asArray()->one()) {
            return Phone::find()->where(['active' => 1, 'deleted' => 0,'contact_id' => $id])->count('id');
        }

        return 0;

    }


    public function edit_contact($model, $create = 0) {
        $data = json_decode(file_get_contents('php://input'), 1);
        //$data = $_POST;

        if (isset($data['first_name'], $data['last_name'], $data['active'])) {

            $model->first_name = Yii::$app->db->quoteValue($data['first_name']);
            $model->first_name = str_replace("'","", $model->first_name);

            $model->last_name = Yii::$app->db->quoteValue($data['last_name']);
            $model->last_name = str_replace("'","", $model->last_name);

            $model->active = intval($data['active']);

            // Length Validation
            if ((strlen($model->first_name) < 3) || (strlen($model->last_name) < 3)) {
                return json_encode(array(
                    'error' => "Error while inserting data. The minimum length of first and last name is 3 symbols",
                    'error_code' => 5
                ));
            }

            if ($create) {
                $model->create_time = strtotime("now");
            } else {
                $model->edit_time = strtotime("now");
            }

            if ($model->save()) {
                return json_encode(array(
                    'success' => "Your data was saved successfully",
                    'contact_id' => $model->id
                ));
            }
            return json_encode(array(
                'error' => "Error adding to database",
                'error_code' => 1
            ));
        }

        return json_encode(array(
            'error' => "Invalid parameters",
            'error_code' => 2
        ));

    }



}
