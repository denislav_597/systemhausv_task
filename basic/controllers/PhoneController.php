<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Phone;
use app\models\Contact;

class PhoneController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionList($contact_id = 0) {
        $contact_id = intval($contact_id);

        if ($contact_id) {
            if (Contact::find()->where(['id' => $contact_id, 'active' => 1, 'deleted' => 0])->asArray()->one()) {
                $phones = Phone::find()->where(['contact_id' => $contact_id, 'active' => 1, 'deleted' => 0])->asArray()->all();
                if ($phones) {
                    return json_encode($phones);
                }
            }
        }
        
        return json_encode(array(
            'error' => 'No records found',
            'error_code' => 4
        ));
    }

  
    public function actionAdd($contact_id = 0) {
        $model = new Phone();
        if ($contact_id) {
            // Check if contact id exists
            if (Contact::find()->where(['id' => $contact_id])->asArray()->one()) {
                return $this->edit_phone($model, 1, $contact_id);
            }
        }

        return json_encode(array(
            'error' => "Invalid parameter contact_id",
            'error_code' => 3
        ));
    }


    public function actionEdit($id = 0) {
        $id = intval($id);
        $model = new Phone();

        if ($id) {
            $model = Phone::find()->where(['id' => $id, 'deleted' => 0])->one();
            if ($model) {
                return $this->edit_phone($model);
            } 

            return json_encode(array(
                'error' => 'Record not found or deleted',
                'error_code' => 4
            ));
        }

        return json_encode(array(
            'error' => "Invalid parameter id",
            'error_code' => 3
        ));
    
    }


    public function actionDelete($id = 0) {
        $id = intval($id);

        if ($id) {
            $model = Phone::find()->where(['id' => $id, 'deleted' => 0])->one();
            if ($model) {
                $model->deleted = 1;
                if ($model->save()) {
                    return json_encode(array(
                        'success' => "Your data was deleted successfully",
                    ));
                }

                return json_encode(array(
                    'error' => "Error while deleting",
                    'error_code' => 1
                ));
            }

            return json_encode(array(
                'error' => 'Record not found or deleted',
                'error_code' => 4
            ));
        }

        return json_encode(array(
            'error' => "Invalid parameter id",
            'error_code' => 3
        ));

    }
    

    public function edit_phone($model, $create = 0, $contact_id = 0) {
        $data = json_decode(file_get_contents('php://input'), 1);
        //$data = $_POST;
        
        if (isset($data['phone_number'], $data['type'], $contact_id, $data['active'])) {

            $model->phone_number = Yii::$app->db->quoteValue($data['phone_number']);
            $model->phone_number = str_replace("'","", $model->phone_number);

            $model->type = Yii::$app->db->quoteValue($data['type']);
            $model->type = str_replace("'","", $model->type);

            $model->active = intval($data['active']);

            // Phone number validation
            if (!is_numeric($model->phone_number)) {
                return json_encode(array(
                    'error' => "Error while inserting data. The phone must include only numbers.",
                    'error_code' => 5
                ));    
            }

            if ($create) {
                $model->create_time = strtotime("now");
                $model->contact_id = intval($contact_id);
            } else {
                $model->edit_time = strtotime("now");
            }

            if ($model->save()) {
                return json_encode(array(
                    'success' => "Your data was saved successfully"
                ));
            }
            return json_encode(array(
                'error' => "Error adding to database",
                'error_code' => 1
            ));
        }
        
        return json_encode(array(
            'error' => "Invalid parameters",
            'error_code' => 2
        ));

    }



}
