<?php

use yii\db\Migration;

/**
 * Class m180228_085530_create_contacts
 */
class m180228_085530_create_contacts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'create_time' => $this->integer()->notNull(),
            'edit_time' => $this->integer(),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->insert('contacts', [
            'first_name' => 'Denis',
            'last_name' => 'Kalfov',
            'create_time' => strtotime("now")
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }

}
