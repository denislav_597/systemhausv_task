<?php

use yii\db\Migration;

/**
 * Class m180228_085605_create_contact_phones
 */
class m180228_085605_create_contact_phones extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contact_phones', [
            'id' => $this->primaryKey(),
            'phone_number' => $this->string(50)->notNull(),
            'type' => $this->string('255')->notNull(),
            'contact_id' => $this->integer()->notNull(),
            'create_time' => $this->integer()->notNull(),
            'edit_time' => $this->integer(),
            'active' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->addForeignKey(
            'fk_contact_phones_contact_id', 'contact_phones', 'contact_id',
            'contacts', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_contact_phones_contact_id', 'contact_phones');
        $this->dropTable('contact_phones');
        
    }

}
