# Backend Requests

# Contacts
## GET contact/list

### Description
* The function returns all active and not deleted contacts

### Parameters
* -

### Returns
* Returns list with all found records or error with error code


## GET contact/list&id=1

### Description
* The function returns the record with passed id only if it is active and not deleted

### Parameters
* [URL] id - Optional

### Returns
* Returns the record with count phone numbers or error with error code


## POST contact/add

### Description
* The function inserts the data to the db

### Parameters
* first_name - Required
* last_name - Required
* active - Required

### Returns
* Success or error with error code


## POST contact/edit&id=1

### Description
* The function edits the data with the new one

### Parameters
* [URL] id - Required
* first_name - Required
* last_name - Required
* active - Required

### Returns
* Success or error with error code


## GET contact/delete&id=1

### Description
* The function updates the field deleted with value 1

### Parameters
* [URL] id - Required

### Returns
* Success or error with error code


# Phones
## GET phone/list&contact_id=1

### Description
* The function returns all active and not deleted mobile phones joined with all active and not deleted contacts

### Parameters
* [URL] contact_id - Required

### Return format
* Returns the record or error with error code


## POST phone/add&contact_id=1

### Description
* The function inserts the data to the db

### Parameters
* phone_number - Required
* type - Required
* active - Required
* [URL] contact_id - Required

### Returns
* Success or error with error code


## GET phone/edit&id=1

### Description
* The function edits the data with the new one

### Parameters
* [URL] id - Required
* phone_number - Required
* type - Required
* active - Required

### Return format
* Returns the record or error with error code


## GET contact/delete&id=1

### Description
* The function updates the field deleted with value 1

### Parameters
* [URL] id - Required

### Returns
* Success or error with error code