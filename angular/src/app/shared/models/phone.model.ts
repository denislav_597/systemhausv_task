export class Phone {  
    id?: number;
    phone_number?: string;
    type?: string;
    contact_id?: number;
    create_time?: number;
    edit_time?: number;
    active?: number;
    deleted?: number;

    success?: string;
    error?: string;
}