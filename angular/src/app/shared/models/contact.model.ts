export class Contact {
    id?: number;
    first_name: string;
    last_name: string;
    phones_count?: number;
    create_time?: number;
    edit_time?: number;
    active: number;
    deleted?: number;

    contact_id?: number;
    success?: string;
    error?: string;
}