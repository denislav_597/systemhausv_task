import { Injectable } from "@angular/core";
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { Contact } from "../models/contact.model";
import { Phone } from "../models/phone.model";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ContactService {
    private url = 'https://carsms.000webhostapp.com/systemhausv_task/web/index.php/?r=contact/';
    private phone_url = 'https://carsms.000webhostapp.com/systemhausv_task/web/index.php/?r=phone/';
    
    constructor(private http: Http) {
    }

    // Get contacts
    getContacts(): Observable<Contact[]> {
        return this.http.get(this.url+"list")
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Get contact by id
    getContact(id: number): Observable<Contact> {
        return this.http.get(this.url+"list&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }
            
    // Create contact
    createContact(contact: Contact): Observable<Contact> {
        return this.http.post(this.url+"add", JSON.stringify(contact))
            .map(res => res.json())
            .catch(this.handleError);
    }
    
    // Update contact
    updateContact(contact: Contact): Observable<Contact> {
        return this.http.post(this.url+"edit/&id="+contact.id, JSON.stringify(contact))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Delete contact
    deleteContact(id: number) {
        return this.http.get(this.url+"delete/&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }


    /* Phone */
        // Get phones by Contact id
        getPhones(id: number): Observable<Phone> {
            return this.http.get(this.phone_url+"list&contact_id="+id)
                .map(res => res.json())
                .catch(this.handleError);
        }
        
        // Add phone
        addPhone(phone: Phone, contact_id: number): Observable<Phone> {
            return this.http.post(this.phone_url+"add&contact_id="+contact_id, JSON.stringify(phone))
                .map(res => res.json())
                .catch(this.handleError);
        }

        // Update phone by phone_id
        updatePhone(phone: Phone): Observable<Phone> {
            return this.http.post(this.phone_url+"edit/&id="+phone.id, JSON.stringify(phone))
                .map(res => res.json())
                .catch(this.handleError);
        }

        // Delete phone
        deletePhone(id: number) {
            return this.http.get(this.phone_url+"delete/&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
        }
    /* Phone end */

    private handleError(err) {
        let errMsg: string;
        
        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
            errMsg = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMsg);
    }
    
}
