import { RouterModule, Routes } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ContactComponent } from "./contact/contact.component";
import { ContactListComponent } from "./contact/contact-list/contact-list.component";
import { ContactCreateComponent } from "./contact/contact-create/contact-create.component";
import { ContactEditComponent } from "./contact/contact-edit/contact-edit.component";




export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/contact',
        pathMatch: 'full'
    },
    {
        path: 'contact',
        component: ContactComponent,
        children: [
            {
                path: '',
                component: ContactListComponent
            },
            {
                path: 'create',
                component: ContactCreateComponent
            },
            {
                path: 'edit/:id',
                component: ContactEditComponent
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);