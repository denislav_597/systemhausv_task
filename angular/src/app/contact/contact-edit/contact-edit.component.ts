import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contact } from '../../shared/models/contact.model';
import { Phone } from "../../shared/models/phone.model";
import { ContactService } from "../../shared/services/contact.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  phone_count: number = 0;
  contact: Contact = { first_name:'', last_name:'', active:1 };
  phone: Phone = {};
  addphone: Phone = {};
  editphone: Phone = {};
  errorMsg: string = '';
  
  contactForm = new FormGroup({
    first_name: new FormControl(null,[Validators.required, Validators.minLength(3)]),
    last_name: new FormControl(null, [Validators.required, Validators.minLength(3)])
  });

  constructor(private service: ContactService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    
    this.service.getContact(id)
      .subscribe(contact => this.contact = contact[0]);
    
    this.service.getPhones(id)
      .subscribe(phone => {
        this.phone = phone;
      });
      
  }

  // update Contact and all Phones
  updateContact() {
    this.errorMsg = '';

    this.service.updateContact(this.contact)
      .subscribe(
        res => {
          if (res.error) {
            this.errorMsg = res.error;
          }
          
          // Phone
          if (res.success) 
          for (let i=0; i < this.contact.phones_count; i++) {
            let editphone_number = $("input.phone_number#"+this.phone[i]['id']).val();
            this.editphone = this.phone;
            this.editphone[i]['phone_number'] = editphone_number;
    
            if (this.editphone[i].id) {
              this.service.updatePhone(this.editphone[i])
              .subscribe(
                res => {
                  if (res.error) {
                    this.errorMsg = res.error;
                  }
                  
                  // Last iteration
                  if (i == (this.contact.phones_count-1)) {
                    if ((this.errorMsg).length == 0) {
                      this.router.navigate(['/contact']);
                    }
                  }
              }),
              err => {
                this.errorMsg = "Error while editing phone numbers...";
              }
            }
          }

          if (res.success) {
            this.router.navigate(['/contact']);
          }

        },
        err => {
          this.errorMsg = "Error while editing contact...";
        }
        
      )

  }
   
  deleteContact(id: number) {
    this.errorMsg = '';

    this.service.deleteContact(id)
      .subscribe(res => {
        if (res.success) {
          this.router.navigate(['/contact']);
        } else {
          this.errorMsg = res.error;
        }
      });
  }

  cancelEdit() {
    this.router.navigate(['/contact']);
  }


  // Phone
  deletePhone(id: number) {
    this.errorMsg = '';

    this.service.deletePhone(id)
      .subscribe(res => {
        if (res.success) {
          for (let i=0; i<this.contact.phones_count;i++) {
            
            if (this.phone[i].id == id) { 
              delete this.phone[i].id;
              delete this.phone[i].phone_number;
              delete this.phone[i].type;
              delete this.phone[i].contact_id;
              delete this.phone[i].create_time;
              delete this.phone[i].edit_time;
              delete this.phone[i].active;
              delete this.phone[i].deleted;
          
              delete this.phone[i].success;
              delete this.phone[i].error;    
            }  
          }
          
          $(".phone_number#"+id+", .delete_number#"+id).css("display","none");
        } else {
          this.errorMsg = res.error;
        }
      });
  }

  addPhoneField() {
    this.phone_count++;
    $(".new_phones").clone().removeClass('new_phones').addClass('added_phone').addClass('phone_'+this.phone_count).css("display", "block").prependTo(".all_new_phones");
  }

  addPhone() {    
    this.errorMsg = '';
    let ph_count = this.phone_count;

    for (let i=1; i<=ph_count; i++) {
      let a = $(".phone_"+i+" .add_phone_number").val();
      if (parseInt(a) != a) {
        this.errorMsg = 'Only numbers are allowed';
      }
    }
    
    if (!this.errorMsg)
    for (let i=1; i<=ph_count; i++) {
      let a = $(".phone_"+i+" .add_phone_number").val();
      
      this.addphone['phone_number'] = a;
      this.addphone['type'] = "Personal";
      this.addphone['active'] = 1;
    
    this.service.addPhone(this.addphone, this.contact.id)
    .subscribe(
      res => {
              
        if (res.error) {
          this.errorMsg = res.error;
        }

        // Last iteration
        if (i == ph_count) {
          if ((this.errorMsg).length == 0) {
            this.phone_count = 0;
            $(".added_phone input.gray").removeClass('gray');
          }
        }
          
      },
      err => {
        this.errorMsg = "Error while adding phone...";
      });
    }

  }


  removePhoneField() {
    this.phone_count--;
    $(".added_phone").first().remove();
  }


}
