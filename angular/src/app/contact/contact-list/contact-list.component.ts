import { Component, OnInit } from '@angular/core';
import { Contact } from '../../shared/models/contact.model';
import { ContactService } from "../../shared/services/contact.service";
//import * as $ from 'jquery';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})

export class ContactListComponent implements OnInit {
  contacts: Contact[];
  errorMsg: string = '';
  
  constructor(private service: ContactService) { }

  // Get contacts
  ngOnInit() {
    this.service.getContacts()
      .subscribe(contacts => this.contacts = contacts);  
  }

  // Delete contact
  deleteContact(id: number) {
    this.errorMsg = '';
    
    this.service.deleteContact(id)
      .subscribe(res => {
        if (res.success) {
          this.ngOnInit();
        } else {
          this.errorMsg = res.error;
        }
      });
  }

}
