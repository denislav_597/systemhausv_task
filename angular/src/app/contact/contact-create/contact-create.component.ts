import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contact } from '../../shared/models/contact.model';
import { ContactService } from "../../shared/services/contact.service";

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {
  add_phone_count: number = 0;
  contact: Contact = { first_name:'', last_name:'', active:1 };
  errorMsg: string = '';
  
  contactForm = new FormGroup({
    first_name: new FormControl(null, [Validators.required, Validators.minLength(3)]),
    last_name: new FormControl(null, [Validators.required, Validators.minLength(3)])
  });

  constructor(private service: ContactService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {      
  }

  createContact() {
    this.errorMsg = '';
    
    this.service.createContact(this.contact)
      .subscribe(
        res => {
          if (res.success) {
            this.router.navigate(['/contact/edit/'+res.contact_id]);
          } else {
            this.errorMsg = res.error;
          }
        },
        err => {
          this.errorMsg = "Error while editing...";
        }
      )
  }

  cancelAdd() {
    this.router.navigate(['/contact']);
  }

}
